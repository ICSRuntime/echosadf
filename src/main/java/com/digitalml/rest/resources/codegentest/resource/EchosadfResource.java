package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.EchosadfService;
	
import com.digitalml.rest.resources.codegentest.service.EchosadfService.RetrieveTestPathReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.EchosadfService.RetrieveTestPathReturnDTO;
import com.digitalml.rest.resources.codegentest.service.EchosadfService.RetrieveTestPathInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.EchosadfService.RetrieveReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.EchosadfService.RetrieveReturnDTO;
import com.digitalml.rest.resources.codegentest.service.EchosadfService.RetrieveInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.EchosadfService.CreateNameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.EchosadfService.CreateNameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.EchosadfService.CreateNameInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Echosadf
	 * #### Echos back every URL, method, parameter and header
Feel free to make a path or an operation and use **Try
Operation** to test it. The echo server will
render back everything.z
sdsf
	 *
	 * @author admin
	 * @version 3
	 *
	 */
	
	@Path("http://mazimi-prod.apigee.net/echo")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class EchosadfResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(EchosadfResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private EchosadfService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.Echosadf.EchosadfServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private EchosadfService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof EchosadfService)) {
			LOGGER.error(implementationClass + " is not an instance of " + EchosadfService.class.getName());
			return null;
		}

		return (EchosadfService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: retrieveTestPath

	Non-functional requirements:
	*/
	
	@GET
	@Path("/test-path/{id}")
	public javax.ws.rs.core.Response retrieveTestPath(
		@PathParam("id")@NotEmpty String id) {

		RetrieveTestPathInputParametersDTO inputs = new EchosadfService.RetrieveTestPathInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			RetrieveTestPathReturnDTO returnValue = delegateService.retrieveTestPath(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieve

	Non-functional requirements:
	*/
	
	@GET
	@Path("/")
	public javax.ws.rs.core.Response retrieve() {

		RetrieveInputParametersDTO inputs = new EchosadfService.RetrieveInputParametersDTO();
		// Prepare and check all input parameters

	
		try {
			RetrieveReturnDTO returnValue = delegateService.retrieve(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createName

	Non-functional requirements:
	*/
	
	@POST
	@Path("/")
	public javax.ws.rs.core.Response createName(
		@QueryParam("name") String name,
		@QueryParam("year") String year) {

		CreateNameInputParametersDTO inputs = new EchosadfService.CreateNameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setName(name);
		inputs.setYear(year);
	
		try {
			CreateNameReturnDTO returnValue = delegateService.createName(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}